using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class Player_Controller : MonoBehaviour
{
    public float walkSpeed = 0.0f;
    public float runSpeed = 0.0f;
    Vector2 moveInput;

    // This is a Property Not a Method
    public float CurrentMoveSpeed
    {
        get
        {
            if (IsMoving)
            {   // If isMoving & isRunning we need Player to go at 'runSpeed'
                if (IsRunning)
                {
                    return runSpeed;
                }
                // If isMoving but not isRunning Player is to go at 'walkSpeed'
                else
                {
                    return walkSpeed;
                }
            }
            // If neither isRunning or isWalking needs to go back to "idle" which is a movementSpeed of 0
            else
            {
                // Idle Speed = 0
                return 0;
            }
        }

    }

    // private var to store "isMoving" variable
    [SerializeField]
    private bool _isMoving = false;

    public bool IsMoving { 
        // This Getter function is returning private bool value whenever we call isMoving(get)
        get
        {
            return _isMoving;
        }

        private set 
        { 
            // Setting value for private _isMoving for the Getter.
            _isMoving = value;
            // Also need to set a value for the Animator so we need to make reference to it (which is 'Animator animator' from below Rigidbody2D rb;
            /* "isMoving" is getting set below  in the OnMove function so it getting set down there also sets it up here. "isMoving" can also be "_isMoving" they are same thing in this instance */
            animator.SetBool(AnimationStrings.isMoving, value);
        }
    }

    [SerializeField]
    private bool _isRunning = false;

    public bool IsRunning
    {

        get { return _isRunning; }

        set { _isRunning = value;
            animator.SetBool(AnimationStrings.isRunning, value);
        }
    }


    /* Also we must ensure Parent object is completely Right or Left bc all child component will follow this as well
     See 24:00 in Part 8 of 2D Platformer Crash Course In Unity on YouTube for better explination*/

    // this is "true" by default bc player by default is facing right
    public bool _isFacingRight = true;


    // This is a Property
    public bool IsFacingRight { get { return _isFacingRight; } private set {

            // if it is = to a new value
            if (_isFacingRight != value)
            {
                // Flip the local scale to make the player face the oppsite direction

                transform.localScale *= new Vector2(-1, 1);     // We are flipping (reversing) the x-axis and leaving y-value alone
            }

            _isFacingRight = value;
        
        } }

    // Reference to our Rigidbody component on our player 
    Rigidbody2D rb;

    Animator animator;


    // Use Awake() to ensure things are called before the Start() has been ran
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        // Getting reference for Animator to be used in the IsRunning and IsMoving functions
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // FixedUpdate is used for physics calculations
    private void FixedUpdate()
    {
        // Sometimes 'Time.fixedDeltaTime' will be multipled (*) after walkSpeed but that is not needed bc "velocity" already handles that internally

        // Since this is a side scroller we do not want 'velocity.y' to change so we leave it as it already was previously by putting "rb.velocity.y"
        // This is the rb.velocity before isRunning and isMoving speeds were a thing new one is below
        /* rb.velocity = new Vector2(moveInput.x * walkSpeed, rb.velocity.y); */

        rb.velocity = new Vector2(moveInput.x * CurrentMoveSpeed, rb.velocity.y);   // CurrentMoveSpeed is decided for the Function Above
    }

    // This is just 'public' for now for testing
    public void OnMove(InputAction.CallbackContext context)
    {
        moveInput = context.ReadValue<Vector2>();

        // This is setting isMoving to true if Vector2.zero (i.e. Vector2 (0,0)) is not zero
        IsMoving = moveInput != Vector2.zero;

        // Set direction player is facing depending on which way they are going
        SetFacingDirection(moveInput);
    }

    private void SetFacingDirection(Vector2 moveInput)
    {
        // 'And' check is to see if player is already facing that direction
        if (moveInput.x > 0 && !IsFacingRight)
        {
            // Face the right
            IsFacingRight = true;
        }

        // 'And' check is to see if player is already facing that direction
        else if (moveInput.x < 0 && IsFacingRight)
        {
            // Face the left
            IsFacingRight = false;
        }
    }

    public void OnRun(InputAction.CallbackContext context)
    {
        // Button just got pressed do this (1st action)
        if (context.started)
        {
            IsRunning = true;
        
        }
        // else if the button got released do this (2nd action)
        else if (context.canceled)
        {
            IsRunning = false;
        }
    }
}
