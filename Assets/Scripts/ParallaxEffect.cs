using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    public Camera cam;
    public Transform followTarget;


    // Starting position for the parallax gameObject
    Vector2 startingPostion;

    // Starting Z Postion
    float startingZ;

    // The => is saying that it is updated every frame so we dont have to put in Update(). Its a shorthand

    /* Distance that the camera has moved from the starting position of the parallax object*/
    Vector2 camMoveSinceStart => (Vector2)cam.transform.position - startingPostion;

    /* */
    float zDistanceFromTarget => transform.position.z - followTarget.transform.position.z;

    /* If object is in front of target, use near clip plane. If behnind target, use farClipPlane*/
    float clippingPlane => (cam.transform.position.z + (zDistanceFromTarget > 0 ? cam.farClipPlane : cam.nearClipPlane));

    /* The further the object from the player, the faster the ParallaxEffect object will move. Drag it's Z value closer to the target to make it slower */
    float parallaxFactor => Mathf.Abs(zDistanceFromTarget) + clippingPlane;

    // Start is called before the first frame update
    void Start()
    {
        // Transform calls for a z-coordinate but since its a vector 2 we dont have that. So, we make a var for z-coord so, it never changes from 0 
        // and we dont get an error
        startingPostion = transform.position;
        startingZ = transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 newPosition = startingPostion + camMoveSinceStart * parallaxFactor;

        transform.position = new Vector3(newPosition.x, newPosition.y, startingZ);
    }
}
